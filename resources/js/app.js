$(function () {
	$('.burger-wrap').click(function(){
		$(this).toggleClass('burger-wrap-active');
		$('.header-menu-xs').slideToggle();
	});
	$('.lifehacks-slider-wrapper').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		dots: false,
		arrows:true,
		lazyLoad: 'ondemand',
		rows: 1,
		autoplay: true,
		cssEase: 'linear',
		prevArrow: $('.main-lifehacks-slider').find('.arrow-prev'),
		nextArrow: $('.main-lifehacks-slider').find('.arrow-next'),
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll:1,
				arrows: false
			}
		},
		{
			breakpoint: 992,
			settings: {
				arrows: false
			}
		},
		]
	})
});