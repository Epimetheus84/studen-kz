<?php

namespace App;

use TCG\Voyager\Models\Post as BasePost;
class Post extends BasePost
{
	public static function findForCategoryByKey($key, $limit, $only_featured=false)
	{
		$where = [
			'category_id' => $key,
			'status'	  => 'published'
		];
		if ($only_featured) $where['featured'] = 1;
		$posts = Post::where($where)->limit($limit)->orderBy('created_at', 'desc')->get();
		return $posts;
	}

	public function getDijestNumber()
	{
		$titles = preg_split("/\s/", $this->title, 2);

		if (!$this->isDijest()) return '';

		if (is_numeric($titles[0]) && intval($titles[0]) > 0) {
			return intval($titles[0]);
		}

		return '';
	}

	public function getDijestTitleWithoutNumber()
	{
		if (!$this->getDijestNumber()) {
			return $this->title;
		}

		$titles = preg_split("/\s/", $this->title, 2);
		return $titles[1];
	}

	public function isDijest()
	{
		return $this->category_id == self::CATEGORY_DIJEST;
	}

	const CATEGORY_LIFEHACK = 1;
	const CATEGORY_DIJEST   = 2;
	const CATEGORY_STORIES  = 3;
	const CATEGORY_NEWS     = 4;

	public function link()
	{
		return '/post/' . $this->id . '-' . $this->slug;
	}
}
