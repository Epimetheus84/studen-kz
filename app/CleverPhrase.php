<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\HockuItem;
class CleverPhrase extends HockuItem
{	
	protected $fillable = [
        'text',
    ];
}