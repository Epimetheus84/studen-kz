<?php 
namespace App\Helpers;

if (!function_exists('get_youtube_video_hash')) {
	function get_youtube_video_hash($video)
	{
		preg_match("#(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^\"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+#", $video, $matches);
		return $matches[0];
	}
}