<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($page_id)
    {
        $page = Page::find($page_id);
        if (!$page) {
        	abort('404');
        }
        return view('page.simple', compact('page'));
    }
}
