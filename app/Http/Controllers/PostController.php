<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($post_id)
    {
        $post = Post::find($post_id);
        if (!$post) {
            abort('404');
        }
        return view('post.simple', compact('post'));
    }

    public function category($slug)
    {
        $page = isset($_GET['page']) && is_available_int($_GET['page'])
            ? intval($_GET['page'])
            : 1;

        $limit = 10;
        $offset = ($page - 1) * $limit;

        $category = Category::where(['slug' => $slug])->first();

        if (!$category) {
            abort('404');
        }

        $posts = Post::where(['category_id' => $category->id])->limit($limit)
               ->offset($offset)
               ->get();

        return view('post.list', compact('posts'));
    }
}
