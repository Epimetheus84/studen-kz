<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Post;	

class DefaultController extends Controller
{
    public function index()
    {
    	$lifehacks = Post::findForCategoryByKey(Post::CATEGORY_LIFEHACK, 10);
    	$dijests   = Post::findForCategoryByKey(Post::CATEGORY_DIJEST, 4);
    	$stories   = Post::findForCategoryByKey(Post::CATEGORY_STORIES, 3);
    	$news_of_day   = setting('site.news_of_day')?Post::find(setting('site.news_of_day')):false;
    	$news_of_week  = setting('site.news_of_week')?Post::find(setting('site.news_of_week')):false;
    	$news_of_month = setting('site.news_of_month')?Post::find(setting('site.news_of_month')):false;
    	$video_of_day  = setting('site.video_of_day')?\App\Video::find(setting('site.video_of_day')):false;
    	$hocku     = \App\HockuItem::getRandom()?nl2br(\App\HockuItem::getRandom()->text):false;
    	$phrase    = \App\CleverPhrase::getRandom()?nl2br(\App\CleverPhrase::getRandom()->text):false;
        return view('default.main', compact('lifehacks', 'dijests', 'hocku', 'stories', 'phrase', 'news_of_day', 'news_of_month', 'news_of_week', 'video_of_day'));
    }
}
