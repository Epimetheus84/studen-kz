<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class HockuItem extends Model
{
	protected $fillable = [
        'text',
    ];

    public static function getRandom()
    {
    	return self::inRandomOrder()->first();
    }
}
