<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index');

Route::get('/post/{id}-{slug}', 'PostController@show');
Route::get('/page/{id}-{slug}', 'PageController@show');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/category/{slug}', 'PostController@category');



